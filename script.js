let courses = [
	{
		id: "1",
		name: "Introduction to HTML",
		description: "A course dealing with HTML.",
		price: 10000,
		isActive: true
	},
	{
		id: "2",
		name: "Introduction to CSS",
		description: "A course that deals with CSS.",
		price: 20000,
		isActive: true
	},
	{
		id: "3",
		name: "Introduction to JS",
		description: "Course that deals with JS.",
		price: 30000,
		isActive: true
	},
	{
		id: "4",
		name: "Introduction to MongoDB",
		description: "Introduce to MongoDB.",
		price: 40000,
		isActive: false
	}
];
let addCourse = (received_id,received_name,received_description,received_price,received_isActive) => {
	courses.push({id: received_id, name: received_name, description: received_description, price: received_price, isActive: received_isActive});
  alert(`You have created ${received_name}. Its price is ${received_price}`);
}
let getSingleCourse = (received_id) => courses.find(subject => subject.id == received_id);
let getAllCourses = () => courses;
let deleteCourse = () => courses.pop();
let showActive = () => courses.filter((subjects) => subjects.isActive == true);
let archiveCourse = (index_num) => courses[index_num].isActive = false;
let archiveCourse2 = (received_name) => courses[courses.findIndex((subject) => subject.name == r_name)].isActive = false;